import 'antd/dist/antd.css'
import React from 'react'
import ReactDOM from 'react-dom'
import './styles/index.scss'
import { BrowserRouter } from 'react-router-dom'
import Routes from './routes'

ReactDOM.render(
  <BrowserRouter>
    <Routes />
  </BrowserRouter>,
  document.getElementById('root'),
)
