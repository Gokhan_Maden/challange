class LocalStorage {
  get = (key) => JSON.parse(localStorage?.getItem(key))

  set = (key, value) => localStorage?.setItem(key, JSON.stringify(value))

  remove = (key, value) => {
    if (value) {
      const item = JSON.parse(localStorage.getItem(key))

      if (item) {
        delete item[value]
      }
      return localStorage.setItem(key, JSON.stringify(item))
    }

    return localStorage.removeItem(key)
  }

  clear = () => localStorage.clear()
}

export default new LocalStorage()
