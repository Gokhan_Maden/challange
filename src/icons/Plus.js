import * as React from 'react'

function SvgPlus(props) {
  return (
    <svg viewBox="0 0 48 48" {...props}>
      <circle fill="#4CAF50" cx={24} cy={24} r={21} />
      <g fill="#fff">
        <path d="M21 14h6v20h-6z" />
        <path d="M14 21h20v6H14z" />
      </g>
    </svg>
  )
}

export default SvgPlus
