import * as React from 'react'

function SvgDelete(props) {
  return (
    <svg height={24} width={24} {...props}>
      <path
        d="M22 13c0 5.523-4.477 10-10 10S2 18.523 2 13 6.477 3 12 3s10 4.477 10 10z"
        fill="#c0392b"
      />
      <path
        d="M22 12c0 5.523-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2s10 4.477 10 10z"
        fill="#e74c3c"
      />
      <path fill="#c0392b" d="M6 11h12v4H6z" />
      <path fill="#ecf0f1" d="M6 11h12v3H6z" />
    </svg>
  )
}

export default SvgDelete
