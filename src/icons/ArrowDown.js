import * as React from 'react'

function SvgArrowDown(props) {
  return (
    <svg viewBox="0 0 32 32" {...props}>
      <path
        clipRule="evenodd"
        d="M26.704 20.393a1.017 1.017 0 00-1.428 0l-8.275 8.193V1c0-.552-.452-1-1.01-1s-1.01.448-1.01 1v27.586l-8.275-8.192a1.016 1.016 0 00-1.428 0 .994.994 0 000 1.414l9.999 9.899c.39.386 1.039.386 1.429 0l9.999-9.899a.994.994 0 00-.001-1.415c-.394-.39.395.391 0 0z"
        fill="#121313"
        fillRule="evenodd"
      />
    </svg>
  )
}

export default SvgArrowDown
