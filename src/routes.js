import React from 'react'
import { Switch, Route } from 'react-router'

import Layout from './components/Layout'
import Home from './pages/Home'
import AddLink from './pages/AddLink'

const Routes = () => (
  <Layout>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/add-link" exact component={AddLink} />
    </Switch>
  </Layout>
)

export default Routes
