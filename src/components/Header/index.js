import React from 'react'
import { useHistory } from 'react-router-dom'

import HepsiBurada from '../../icons/Hepsiburada'

function Header() {
  const history = useHistory()

  return (
    <header className="header">
      <div className="header-logo" onClick={() => history.push('/')}>
        <HepsiBurada />
      </div>
      <div className="header-title">Link Vote Challange</div>
    </header>
  )
}

export default Header
