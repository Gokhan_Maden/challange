import React from 'react'
import { shallow } from 'enzyme'

import Header from './index'

describe('MyComponent', () => {
  test('renders', () => {
    const wrapper = shallow(<Header />)

    expect(wrapper.exists()).toBe(true)
  })
})
