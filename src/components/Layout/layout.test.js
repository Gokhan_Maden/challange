import React from 'react'
import { shallow } from 'enzyme'

import Layout from './index'

describe('MyComponent', () => {
  test('renders', () => {
    const wrapper = shallow(<Layout />)

    expect(wrapper.exists()).toBe(true)
  })

  test('should render correctly in "debug" mode', () => {
    const children = <div>HepsiBurada Challange</div>
    const component = shallow(<Layout {...children} />)

    expect(component).toMatchSnapshot()
  })
})
