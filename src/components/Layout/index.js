import React from 'react'
import PropTypes from 'prop-types'
import Header from '../Header'

const Layout = ({ children }) => (
  <div className="layout">
    <Header />
    <div className="page">{children}</div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
