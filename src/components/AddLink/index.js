import React from 'react'

import Plus from '../../icons/Plus'

const AddLink = () => (
  <div className="add-link">
    <div className="add-link-plus">
      <div className="plus-icon">
        <Plus />
      </div>
    </div>
    <span className="add-link-info">Submit A Link</span>
  </div>
)

export default AddLink
