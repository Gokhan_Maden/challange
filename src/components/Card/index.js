import React, { useState } from 'react'
import { Modal } from 'antd'
import PropTypes from 'prop-types'

import ArrowDown from '../../icons/ArrowDown'
import ArrowUp from '../../icons/ArrowUp'
import Delete from '../../icons/Delete'

const Card = ({ listItem, index, remove, up, down }) => {
  const [visible, setVisible] = useState(false)

  const showModal = () => {
    setVisible(true)
  }

  const handleOk = () => {
    setVisible(false)
    remove(index)
  }

  const handleCancel = () => {
    setVisible(false)
  }

  return (
    <div className="card">
      <div className="card-active" data-test="card-active" onClick={showModal}>
        <div className="delete-icon">
          <Delete />
        </div>
      </div>
      <div className="link-vote">
        <span className="vote-counter">{listItem.vote}</span>
        <span>Points</span>
      </div>
      <div className="link-content">
        <div className="link-info">
          <span className="link-title">{listItem.linkName}</span>
          <span className="link">({listItem.linkUrl})</span>
        </div>
        <div className="link-interactions">
          <div
            className="link-up-vote"
            data-test="link-up-vote"
            onClick={() => up(listItem, index)}
          >
            <div className="arrow-up-icon">
              <ArrowUp />
            </div>
            <span>Up Vote</span>
          </div>
          <div className="link-down-vote" onClick={() => down(listItem, index)}>
            <div className="arrow-down-icon">
              <ArrowDown />
            </div>
            <span>Down Vote</span>
          </div>
        </div>
      </div>
      <Modal
        title="Remove Link"
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
        className="approval-modal"
      >
        <span>Do you want to remove:</span>
        <span>{listItem.linkName}</span>
      </Modal>
    </div>
  )
}

Card.propTypes = {
  listItem: PropTypes.object,
  index: PropTypes.number,
  remove: PropTypes.func,
  up: PropTypes.func,
  down: PropTypes.func,
}

Card.defaultProps = {
  listItem: {},
  index: 0,
  remove: () => {},
  up: () => {},
  down: () => {},
}

export default Card
