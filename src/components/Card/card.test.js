import React from 'react'
import Enzym, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Card from './index'

Enzym.configure({ adapter: new Adapter() })

const initialState = {
  id: '72c24c9d-db77-489e-b131-6176644d9b29',
  linkName: 'Gokhan Maden',
  linkUrl: 'gokhanmaden.com',
  vote: 25,
}

describe('Card', () => {
  test('check card component', () => {
    const wrapper = shallow(<Card listItem={initialState} />)

    expect(wrapper.exists()).toBe(true)
  })

  test('should render correctly in "debug" mode', () => {
    const component = shallow(<Card debug listItem={initialState} />)

    expect(component).toMatchSnapshot()
  })

  test('check hover state of card component', () => {
    const wrapper = shallow(<Card listItem={initialState} />)
    expect(wrapper.find('.card').exists).toBeTruthy()

    wrapper.simulate('mouseover')
    expect(wrapper.find('.card-active')).toBeTruthy()
  })
})
