import React from 'react'
import { shallow } from 'enzyme'

import Home from './index'

describe('MyComponent', () => {
  test('renders', () => {
    const wrapper = shallow(<Home />)

    expect(wrapper.exists()).toBe(true)
  })

  test('should render correctly in "debug" mode', () => {
    const component = shallow(<Home debug />)

    expect(component).toMatchSnapshot()
  })
})
