import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Select, message, Pagination } from 'antd'

import Card from '../../components/Card'
import AddLink from '../../components/AddLink'
import LocalStorage from '../../utils/localstorage'

function Home() {
  const history = useHistory()
  const { Option } = Select
  const numberOfEachPage = 5

  if (!LocalStorage.get('voteList')) {
    const voteList = []
    LocalStorage.set('voteList', voteList)
  }

  const [voteList, setVoteList] = useState(LocalStorage.get('voteList'))
  const [minValue, setMinValue] = useState(0)
  const [maxValue, setMaxValue] = useState(5)
  const [count, setCount] = useState(0)

  useEffect(() => {}, [voteList])

  const compare = (a, b) => {
    const bandA = a.vote
    const bandB = b.vote

    let comparison = 0
    if (bandA < bandB) {
      comparison = 1
    } else if (bandA > bandB) {
      comparison = -1
    }
    return comparison
  }

  const handleChange = (value) => {
    if (value === 'most') {
      voteList.sort(compare)
      setCount(count + 1)
      return
    }

    voteList.reverse(compare)
    setCount(count + 1)
  }

  const removeItemHandler = (index) => {
    if (LocalStorage.get('voteList')[index]) {
      const removeItem = LocalStorage.get('voteList')[index]
      const filteredList = voteList.filter((link) => link.id !== removeItem.id)

      LocalStorage.set('voteList', filteredList)
      setVoteList(filteredList)
    }

    message.success('BAŞARI İLE SİLİNDİ.')
  }

  const voteHandler = (item, index, list) => {
    list[index] = item
    setVoteList(list)
    setCount(count + 1)
    list.sort(compare)
  }

  const upVote = (item, index) => {
    item.vote += 1
    voteHandler(item, index, voteList)
    return LocalStorage.set('voteList', voteList)
  }

  const downVote = (item, index) => {
    item.vote -= 1
    voteHandler(item, index, voteList)
    return LocalStorage.set('voteList', voteList)
  }

  const handleChangePage = (value) => {
    const newMinValue = (value - 1) * numberOfEachPage
    const newMaxValue = value * numberOfEachPage

    setMinValue(newMinValue)
    setMaxValue(newMaxValue)
  }

  return (
    <div className="home">
      <div onClick={() => history.push('/add-link')}>
        <AddLink />
      </div>
      <Select
        defaultValue="Order by"
        onChange={handleChange}
        className="sort-by-order"
      >
        <Option value="most">Most Voted</Option>
        <Option value="less">Less Voted</Option>
      </Select>
      {voteList?.slice(minValue, maxValue).map((listItem, index) => (
        <Card
          listItem={listItem}
          index={index}
          key={index}
          remove={removeItemHandler}
          up={upVote}
          down={downVote}
        />
      ))}
      {voteList?.length > 5 ? (
        <Pagination
          defaultCurrent={1}
          defaultPageSize={numberOfEachPage} // default size of page
          onChange={handleChangePage}
          total={voteList?.length} // total number of card data available
        />
      ) : null}
    </div>
  )
}

export default Home
