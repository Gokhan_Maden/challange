import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid'

import { Form, Input, Button, message } from 'antd'
import LocalStorage from '../../utils/localstorage'
import ArrowLeft from '../../icons/ArrowLeft'

function AddLink() {
  const history = useHistory()
  const [voteList, setVoteList] = useState(LocalStorage.get('voteList'))

  const onFinish = (values) => {
    const voteItem = {
      linkName: values.linkName,
      linkUrl: values.linkUrl,
      id: uuidv4(),
      vote: 0,
    }

    const newVoteList = [voteItem, ...voteList]
    setVoteList(newVoteList)
    LocalStorage.set('voteList', newVoteList)
    history.push('/')
    message.success(`${values.linkName} ADDED`)
  }

  const onFinishFailed = () => {
    message.error('Please fill required fields!')
  }

  return (
    <div className="add-link-page">
      <div className="redirect-home" onClick={() => history.push('/')}>
        <div className="arrow-left-icon">
          <ArrowLeft />
        </div>
        <span>Return to List</span>
      </div>

      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Link Name"
          name="linkName"
          rules={[
            {
              required: true,
              message: 'Please input your linkName!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Link Url"
          name="linkUrl"
          rules={[
            {
              required: true,
              message: 'Please input your linkUrl!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default AddLink
