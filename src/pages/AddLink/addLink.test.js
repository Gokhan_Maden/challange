import React from 'react'
import { shallow } from 'enzyme'

import AddLink from './index'

describe('MyComponent', () => {
  test('renders', () => {
    const wrapper = shallow(<AddLink />)

    expect(wrapper.exists()).toBe(true)
  })

  test('should render correctly in "debug" mode', () => {
    const component = shallow(<AddLink debug />)

    expect(component).toMatchSnapshot()
  })
})
