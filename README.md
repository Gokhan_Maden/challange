## Hepsiburada Link Vote Challange

### Getting Started

The project runs on [Node][node] version > 14.x, make sure your version is up to date if it doesn't work.

### Basic starting commands to run

```bash
git clone https://github.com/GokhanMaden/link-vote-challange

cd ./link-vote-challange

npm install

npm run start
```

### Linters

Eslint and prrettier is used with conjunction to work together. You can test with following command;

```bash
npm run lint
```

Also with this command you can fix linter errors and format all pieces of code in the entire project

```bash
npm run lint:fix

npm run format
```

### Tests

Tests in the project were written using Jest and Enzyme. You can see the status and success of the written tests with the commands below;

```bash
npm run test

npm run test:coverage
```

After the command we ran to measure the code coverage rate (npm run test:coverage), where the project is located 'coverage' file will appear.
If you open this file with browser, you will see whole result of tests with details. link-vote-challange/coverage/lcov-report/index.html 

Made with <3 by Gökhan Maden